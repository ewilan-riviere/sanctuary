# Sanctuary

[![php](https://img.shields.io/static/v1?label=PHP&message=v7.4&color=777bb4&style=flat-square&logo=php&logoColor=ffffff)](https://www.php.net)
[![composer](https://img.shields.io/static/v1?label=Composer&message=v2.0&color=885630&style=flat-square&logo=composer&logoColor=ffffff)](https://getcomposer.org)  
[![laravel](https://img.shields.io/static/v1?label=Laravel&message=8.0&color=ff2d20&style=flat-square&logo=laravel&logoColor=ffffff)](https://laravel.com)
[![swagger](https://img.shields.io/static/v1?label=Swagger&message=v3.0&color=85EA2D&style=flat-square&logo=swagger&logoColor=ffffff)](https://swagger.io)

[![vue](https://img.shields.io/static/v1?label=Vue.js&message=v2.6&color=4FC08D&style=flat-square&logo=vue.js&logoColor=ffffff)](https://vuejs.org/)
<!-- [![nuxtjs](https://img.shields.io/static/v1?label=NuxtJS&message=v2.14&color=00C58E&style=flat-square&logo=nuxt.js&logoColor=ffffff)](https://nuxtjs.org/) -->
[![tailwindcss](https://img.shields.io/static/v1?label=Tailwind%20CSS&message=v2.0&color=38B2AC&style=flat-square&logo=tailwind-css&logoColor=ffffff)](https://tailwindcss.com/)
[![nodejs](https://img.shields.io/static/v1?label=NodeJS&message=14.15&color=339933&style=flat-square&logo=node.js&logoColor=ffffff)](https://nodejs.org/en)
[![yarn](https://img.shields.io/static/v1?label=Yarn&message=v1.2&color=2C8EBB&style=flat-square&logo=yarn&logoColor=ffffff)](https://yarnpkg.com/lang/en/)

This is a protoapp to use **Laravel Sanctum** authentication with Vue.js SPA app, you can find [documentation here](https://laravel.com/docs/8.x/sanctum).

Setup Vue & Laravel apps.

```bash
php setup.php
```

In `client/`

```bash
yarn dev
```

In `api/`

```bash
php artisan serve
```

- <https://auth.nuxtjs.org/providers/laravel-sanctum>
- <https://laravel.com/docs/8.x/sanctum>
- <https://laravel-news.com/laravel-auth-tips>
- <https://github.com/fruitcake/laravel-cors>
- <https://laracasts.com/discuss/channels/laravel/sancton-cors-issue>
- <https://stackoverflow.com/questions/62115781/laravel-sanctum-blocked-by-cors-policy-with-nuxt-auth-module>
- <https://dev.to/mandeepm91/how-to-add-authentication-to-your-universal-nuxt-app-using-nuxt-auth-module-3ffm>
- <https://www.digitalocean.com/community/tutorials/implementing-authentication-in-nuxtjs-app>
- <https://github.com/nuxt-community/auth-module/issues/731>
- <https://laravel.com/docs/8.x/sanctum#protecting-mobile-api-routes>
- <https://auth.nuxtjs.org/>
- <https://www.youtube.com/watch?v=2bslfjKEAik>
- <https://github.com/qirolab/laravel-sanctum-example/blob/api-token-authentication/vue-app/src/views/Register.vue>
