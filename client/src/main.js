import Vue from "vue";
import "@/plugins/axios";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import "./assets/css/tailwind.css";
import "@/plugins/vue-cookies";
import "@/plugins/global-loader";
import "@/plugins/icons-loader";

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
