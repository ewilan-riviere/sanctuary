import Vue from "vue";
import router from "../router";
import Vuex from "vuex";
import VueCookies from "vue-cookies";
import VueSession from "vue-session";

Vue.use(Vuex);
Vue.use(VueCookies);
Vue.use(VueSession);
const cookie = Vue.$cookies;
const session = Vue.prototype.$session;
cookie.config("30d", "", "", true);

const store = new Vuex.Store({
  state: {
    authenticated: false,
    user: null,
    key: null,
    dataType: null
  },
  mutations: {
    setAuth(state, { user, remember }) {
      // localStorage.setItem('sanctuary_auth', JSON.stringify(user));
      // cookie.set("key", "Hello world!", 1);
      // cookie.get("key");
      // cookie.delete("key");

      this.initCookie();
      state.authenticated = true;
      state.user = user;
      if (remember) {
        state.dataType = this.cookie;
        cookie.set("sanctuary_auth", user);
      } else {
        session.set("sanctuary_auth", user);
      }
    },
    logout(state) {
      // localStorage.removeItem('sanctuary_auth');
      cookie.remove("sanctuary_auth");
      cookie.remove("XSRF-TOKEN");
      cookie.remove("markdownthis_session");
      state.authenticated = false;
      state.user = null;
      session.clear();
      router.push("login");
    },
    // eslint-disable-next-line no-unused-vars
    initializeAuth(state) {
      this.initCookie();
      // state.user = cookie.get('auth');
      // if (localStorage.getItem('sanctuary_auth')) {
      //   state.authenticated = true;
      //   state.user = JSON.parse(localStorage.getItem('sanctuary_auth'));
      // }
    }
  },
  getters: {
    auth(state) {
      return state.authenticated;
    }
  },
  actions: {},
  modules: {}
});

// eslint-disable-next-line no-unused-vars
store.initCookie = () => {
  store.state.dataType = store.session;
  if (cookie.get("sanctuary_auth")) {
    store.state.dataType = store.cookie;
    store.state.authenticated = true;
    store.state.user = cookie.get("sanctuary_auth");
  } else if (session.get("sanctuary_auth")) {
    store.state.authenticated = true;
    store.state.user = session.get("sanctuary_auth");
  }
};

store.session = "⏳ session";
store.cookie = "🍪 cookie";

export default store;
