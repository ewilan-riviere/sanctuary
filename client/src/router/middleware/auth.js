export default function auth({ next, store }) {
  if (!store.state.authenticated) {
    return next({
      name: "Login"
    });
  }

  return next();
}
