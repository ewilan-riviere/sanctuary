export default function guest({ next, store }) {
  if (store.state.authenticated) {
    return next({
      name: "Dashboard"
    });
  }

  return next();
}
