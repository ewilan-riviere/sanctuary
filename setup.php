<?php

function br()
{
    echo "\n";
}

chdir("client");
br();
echo "Vue Client setup";
br();
$client_yarn = exec('yarn');
echo "Node.js dependencies... $client_yarn";
br();
chdir("..");
chdir("api");
br();
echo "Laravel API setup";
exec('composer install');
$api_yarn = exec('yarn');
